package com.cloud.platform.converter;

import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.cloud.platform.util.DateUtil;

public class TimeConverter implements Converter {

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		return DateUtil.getTimeStr((Date) value);
	}
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return null;
	}
}
