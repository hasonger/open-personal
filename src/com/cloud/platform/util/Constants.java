package com.cloud.platform.util;

import java.util.UUID;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class Constants {

	public static final String VALID_YES = "Y";
	public static final String VALID_NO = "N";
	
	public static final String LOGIN_USER = "LOGIN_USER";
	public static final String LOGIN_USERNAME = "LOGIN_USERNAME";
	
	public static String getID() {
		return UUID.randomUUID().toString();
	}
	
	@SuppressWarnings("deprecation")
	public static String getBasePath() {
		
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		
		return request.getRealPath("/");
	}
	
	public static String getAppPath() {
		
		return getBasePath() + "app\\";
	}
	
	public static String getUploadPath() {
		
		return getBasePath() + "upload\\";
	}
} 
