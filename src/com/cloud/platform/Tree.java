package com.cloud.platform;

public class Tree {

	private String id;
	private String parentId;
	
	private int level;
	private int pad;
	private boolean hasChild;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getPad() {
		return pad;
	}
	
	public void setPad(int pad) {
		this.pad = pad;
	}

	public boolean isHasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}
}
