package com.cloud.knowledge.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloud.knowledge.model.Know;
import com.cloud.platform.IDao;
import com.cloud.platform.json.JSONArray;
import com.cloud.platform.json.JSONObject;
import com.cloud.platform.util.Constants;
import com.cloud.platform.util.SnUtil;
import com.cloud.platform.util.StringUtil;
import com.cloud.platform.util.TreeUtil;

@Component
public class KnowService {

	@Autowired
	private IDao dao;
	
	/**
	 * save knows info for knows manage
	 * 
	 * @param userId
	 * @param knowledgeId
	 * @param knowInfo
	 * @throws Exception
	 */
	public void saveKnowInfo(String userId, String knowledgeId, String knowInfo)
			throws Exception {
		if(StringUtil.isNullOrEmpty(knowInfo)) {
			return;
		}
		
		JSONArray info = new JSONArray(knowInfo);
		
		// iterate every know info
		for(int i = 0; i < info.length(); i++) {
			JSONObject know = info.getJSONObject(i);
			Know k = null;
			
			// add know
			if(StringUtil.isNullOrEmpty(know.getString("i"))) {
				k = new Know();
				k.setId(Constants.getID());
				k.setUid(userId);
				k.setKnowledgeId(knowledgeId);
				
				k.setIsNode(Constants.VALID_YES);
				k.setIsValid(Constants.VALID_YES);
				k.setCreateTime(new Date());
			}
			// modify know or delete know
			else {
				k = (Know) dao.getObject(Know.class, know.getString("i"));
				
				if(know.length() == 1) {  // if info only has id, is delete
					k.setIsValid(Constants.VALID_NO);
				}
			}
			
			// know name
			if(know.has("n")) {
				k.setName(know.getString("n"));
			}
			
			// know parent node id
			if(know.has("pi")) {
				k.setParentId(know.getString("pi"));
			}
			
			// know color
			if(know.has("c")) {
				k.setColor(know.getString("c"));
			}
			
			// know sn
			if(know.has("snid")) {
				k.setSn(SnUtil.getSn("Know", know.getString("snid"), "knowledgeId", knowledgeId));
			}
			
			k.setModifyTime(new Date());
			
			dao.saveObject(k);
		}
	}
	
	/**
	 * search knows
	 * 
	 * @param knowledgeId
	 * @param isValid
	 * @return
	 */
	public List<Know> searchKnows(String knowledgeId, String isValid) {
		if(StringUtil.isNullOrEmpty(knowledgeId)) {
			return new ArrayList();
		}
		
		String hql = "from Know where knowledgeId = ? and isValid = ? order by sn,createTime";
		List knows = dao.getAllByHql(hql, new Object[] {knowledgeId, isValid});
		
		return TreeUtil.sortTree(knows);
	}
	
	/**
	 * save know
	 * 
	 * @param know
	 * @param userId
	 * @param knowledgeId
	 * @param snid
	 */
	public void saveKnow(Know know, String userId, String knowledgeId, String snid) {
		if(know == null) {
			return;
		}
		
		// set attribute when create
		if(StringUtil.isNullOrEmpty(know.getId())) {
			know.setId(Constants.getID());
			know.setUid(userId);
			know.setKnowledgeId(knowledgeId);
			know.setIsNode(Constants.VALID_NO);
			know.setIsValid(Constants.VALID_YES);
			know.setCreateTime(new Date());
			
			know.setSn(SnUtil.getSn("Know", snid, "knowledgeId", knowledgeId));
		}
		
		know.setModifyTime(new Date());
		
		dao.saveObject(know);
	}
	
	/**
	 * get know by know id
	 * 
	 * @param knowId
	 * @return
	 */
	public Know getKnow(String knowId) {
		if(StringUtil.isNullOrEmpty(knowId)) {
			return new Know();
		}
		
		return (Know) dao.getObject(Know.class, knowId);
	}
}
