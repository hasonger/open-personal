package com.cloud.knowledge.model;

import java.util.Date;

import com.cloud.platform.Tree;

public class Knowledge extends Tree {

	private String uid;
	private String name;
	
	private int sn;
	private String color;
	private String isNode;
	
	private Date createTime;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSn() {
		return sn;
	}

	public void setSn(int sn) {
		this.sn = sn;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIsNode() {
		return isNode;
	}

	public void setIsNode(String isNode) {
		this.isNode = isNode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
