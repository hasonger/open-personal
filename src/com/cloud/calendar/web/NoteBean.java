package com.cloud.calendar.web;

import java.util.List;

import net.sf.json.JSONArray;

import com.cloud.calendar.model.Note;
import com.cloud.calendar.service.NoteService;
import com.cloud.platform.BaseHandler;

public class NoteBean extends BaseHandler {

	private NoteService noteService;
	
	private String noteInfo;
	
	/**
	 * get note info
	 * 
	 * @return
	 */
	public String getNoteInfo() {
		
		if(noteInfo == null) {
			try {
				// search user's all notes
				List<Note> notes = noteService.searchNotes(getLoginUserId());
				
				// convert notes list to json
				JSONArray arr = JSONArray.fromObject(notes);
				
				noteInfo = arr.toString();
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return noteInfo;
	}

	/**
	 * save note when create note for dwr
	 * 
	 * @param text
	 * @param left
	 * @param top
	 * @param color
	 */
	public String dynSaveNote(String text, String left, String top, String color, String size) {
		
		String newId = "";
		
		try {
			newId = noteService.saveNote(getLoginUserId(), text, left, top, color, size);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return newId;
	}
	
	/**
	 * update note's position when move note for dwr
	 * 
	 * @param id
	 * @param left
	 * @param top
	 */
	public void dynUpdateNote(String id, String left, String top) {
		
		try {
			noteService.updateNote(id, left, top);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * remove note for dwr
	 * 
	 * @param id
	 */
	public void dynRemoveNote(String id) {
		
		try {
			noteService.removeNote(id);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public NoteService getNoteService() {
		return noteService;
	}

	public void setNoteService(NoteService noteService) {
		this.noteService = noteService;
	}
	
	public void setNoteInfo(String noteInfo) {
		this.noteInfo = noteInfo;
	}
}
