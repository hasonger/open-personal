package com.cloud.goal.web;

import java.util.List;

import com.cloud.goal.model.Task;
import com.cloud.goal.model.TaskReport;
import com.cloud.goal.service.TaskService;
import com.cloud.platform.BaseHandler;
import com.cloud.platform.util.DateUtil;
import com.cloud.platform.util.StringUtil;

public class TaskBean extends BaseHandler {

	private TaskService taskService;
	
	private String taskId;
	private Task task;
	private List<Task> tasks;  // for plan tasks
	private List<Task> mytasks;  // for my tasks
	private List<Task> finishTasks;  // for finish tasks
	private TaskReport taskReport;
	private List<TaskReport> taskReports;
	private float totalEffort;
	
	/**
	 * create task
	 */
	public void createTask() {
		try {
			String planId = getParameter("planId");
			String priority = getParameter("priority");
			String startDate = getParameter("startDate");
			String endDate = getParameter("endDate");
			
			if(StringUtil.isNullOrEmpty(planId)) {
				return;
			}
			
			task.setPlanId(planId);
			task.setPriority(StringUtil.isNullOrEmpty(priority) ? 0 : Integer.parseInt(priority));
			task.setStartDate(DateUtil.parseDate(startDate));
			task.setEndDate(DateUtil.parseDate(endDate));
			
			taskService.createTask(task, getLoginUserId());
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get task's total effort
	 * 
	 * @return
	 */
	public float getTotalEffort() {
		if(totalEffort == 0) {
			try {
				totalEffort = taskService.getTaskTotalEffort(getParameter("taskId"));
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return totalEffort;
	}

	/**
	 * get finish tasks
	 * 
	 * @return
	 */
	public List<Task> getFinishTasks() {
		if(finishTasks == null) {
			try {
				finishTasks = taskService.searchFinishTasks(getLoginUserId());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return finishTasks;
	}
	
	/**
	 * get task's all reports
	 * 
	 * @return
	 */
	public List<TaskReport> getTaskReports() {
		if(taskReports == null) {
			try {
				taskReports = taskService.searchTaskReport(getParameter("taskId"));
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return taskReports;
	}

	/**
	 * save task report
	 */
	public void saveTaskReport() {
		try {
			taskService.saveTaskReport(taskReport, getParameter("taskId"));
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get current day's task report
	 * 
	 * @return
	 */
	public TaskReport getTaskReport() {
		if(taskReport == null) {
			try {
				taskReport = taskService.getTaskReport(getParameter("taskId"));
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return taskReport;
	}

	/**
	 * search my tasks list
	 * 
	 * @return
	 */
	public List<Task> getMytasks() {
		if(mytasks == null) {
			try {
				mytasks = taskService.searchMyTasks(getLoginUserId());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return mytasks;
	}

	/**
	 * save task info from tasks manage
	 */
	public void saveTask() {
		try {
			taskService.saveTask(getParameter("taskInfo"));
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * search plan tasks list
	 * 
	 * @return
	 */
	public List<Task> getTasks() {
		if(tasks == null) {
			try {
				tasks = taskService.searchPlanTasks(getParameter("planId"));
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return tasks;
	}

	/**
	 * save task
	 */
	public void save() {
		try {
			String userId = getLoginUserId();
			String planId = getParameter("planId");
			String info = getParameter("taskInfo");
			
			// set task id for return to page
			taskId = taskService.saveTaskInfo(userId, planId, info);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get task by task id
	 * 
	 * @return
	 */
	public Task getTask() {
		if(task == null) {
			task = taskService.getTask(getParameter("taskId"));
		}
		return task;
	}

	/**
	 * =======================  getters and setters  =======================
	 */
	public void setTask(Task task) {
		this.task = task;
	}
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
	public void setMytasks(List<Task> mytasks) {
		this.mytasks = mytasks;
	}
	
	public void setTaskReport(TaskReport taskReport) {
		this.taskReport = taskReport;
	}
	
	public void setTaskReports(List<TaskReport> taskReports) {
		this.taskReports = taskReports;
	}
	
	public void setFinishTasks(List<Task> finishTasks) {
		this.finishTasks = finishTasks;
	}
	
	public void setTotalEffort(float totalEffort) {
		this.totalEffort = totalEffort;
	}
}
