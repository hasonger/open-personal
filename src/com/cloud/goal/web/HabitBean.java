package com.cloud.goal.web;

import java.util.List;

import com.cloud.goal.model.Habit;
import com.cloud.goal.service.HabitService;
import com.cloud.platform.BaseHandler;
import com.cloud.platform.util.StringUtil;

public class HabitBean extends BaseHandler {

	private HabitService habitService;
	
	private Habit habit;
	private List<Habit> habits;
	
	private String dailyInfo;
	
	/**
	 * get today's daily habit info
	 * 
	 * @return
	 */
	public String getDailyInfo() {
		
		if(dailyInfo == null) {
			try {
				List<String> dailyHabitIds = habitService.searchDailyInfo(getLoginUserId());

				dailyInfo = StringUtil.combineByComma(dailyHabitIds);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return dailyInfo;
	}

	/**
	 * save daily habit
	 */
	public void saveDailyHabit() {
		
		try {
			habitService.saveDailyHabit(getLoginUserId(), dailyInfo);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * save habit
	 */
	public void saveHabit() {
		
		try {
			habitService.saveHabit(getLoginUserId(), habit);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get specific habit
	 * 
	 * @return
	 */
	public Habit getHabit() {
		if(habit == null) {
			habit = new Habit();
		}
		
		return habit;
	}

	/**
	 * get user's all habits
	 * 
	 * @return
	 */
	public List<Habit> getHabits() {
		if(habits == null) {
			try {
				habits = habitService.searchHabits(getLoginUserId());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return habits;
	}

	/**
	 * =======================  getters and setters  =======================
	 */
	public void setHabits(List<Habit> habits) {
		this.habits = habits;
	}
	
	public void setHabit(Habit habit) {
		this.habit = habit;
	}

	public HabitService getHabitService() {
		return habitService;
	}

	public void setHabitService(HabitService habitService) {
		this.habitService = habitService;
	}
	
	public void setDailyInfo(String dailyInfo) {
		this.dailyInfo = dailyInfo;
	}
}
