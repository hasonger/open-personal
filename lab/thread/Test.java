package thread;

public class Test {

	public static void main(String[] args) {

		SynClass synClass = new SynClass();
		
		Thread t1 = new Thread(new Thread1(synClass));
		t1.start();
		
		Thread t2 = new Thread(new Thread2(synClass));
		t2.start();
	}
}
