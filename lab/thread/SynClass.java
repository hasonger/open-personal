package thread;

public class SynClass {

	public synchronized void print() {
		System.out.println("SynClass print...........");
		
		System.out.println("sleep 10 seconds........");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("sleep over........");
	}
	
	public synchronized static void staticPrint() {
		System.out.println("SynClass static print..........");
		
		System.out.println("sleep 10 seconds........");
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void say() {
		System.out.println("SynClass say...........");
	}
}
