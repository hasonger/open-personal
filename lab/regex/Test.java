package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	public static void main(String[] args) {

		String input = "yuzhengzhong....";
		
		Pattern pattern = Pattern.compile("^[y]");
		Matcher matcher = pattern.matcher(input);
		
		boolean result = matcher.find();
		
		System.out.println(result);
	}

}
