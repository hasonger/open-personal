<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/json2.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/dwr/engine.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/util.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/__goalBean.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/__planBean.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="createTask();return false;">保存</a>
		<a href="#" onclick="clearPage('taskFrm');return false;">取消</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">任务名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText id="name" value="#{taskBean.task.name}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">所属目标</td>
				<td class="edit-whole" colspan="3">
					<div id="goalSel" class="select-div"></div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">所属计划</td>
				<td class="edit-whole" colspan="3">
					<div id="planSel" class="select-div">
						<ul class="menu-ul"><li>请选择</li></ul>
					</div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">优先级</td>
				<td class="edit-whole" colspan="3">
					<div id="priority" class="select-div">
						<ul class="menu-ul">
							<li>请选择</li>
							<li val="1">高</li>
							<li val="2">中</li>
							<li val="3">低</li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">起始日期</td>
				<td class="edit-whole" colspan="3">
					<input id="startDate" name="startDate" class="edit-timer" value="<h:outputText value="#{taskBean.task.startDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">结束日期</td>
				<td class="edit-whole" colspan="3">
					<input id="endDate" name="endDate" class="edit-timer" value="<h:outputText value="#{taskBean.task.endDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">任务描述 </td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea id="remark" value="#{taskBean.task.remark}" style="height: 160px;" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">验收标准</td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea id="acceptance" value="#{taskBean.task.acceptance}" style="height: 160px;" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" id="_planId" name="planId" />
		<input type="hidden" id="_priority" name="priority" />
		<a4j:commandLink id="save" action="#{taskBean.createTask}" oncomplete="clearPage('taskFrm', true);" />
	</h:form>
	
	<script>
		var info;
		$("#startDate, #endDate").omCalendar();
		
		// init selects
		$("#priority, #planSel").select();
		
		$("#goalSel").select({
			load: function() {
				var arr;
				
				__goalBean.dynGetGoals({
					async: false,
					callback: function(data) { arr = eval(data); }
				});
				
				return arr;
			},
			onchange: function(val) {
				__planBean.dynGetPlans(val, function(data) {
					var arr = eval(data);
					$("#planSel").selectRefresh(arr);
				});
			} 
		});
		
		function createTask() {
			$("#_planId").val($("#planSel").selectVal());
			$("#_priority").val($("#priority").selectVal());
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>