<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/jquery-ui.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/colorpicker.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-ui.min.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/json2.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-menu.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/colorpicker.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.table.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="clearPage('tasksFrm');return false;">返回</a>
	</div>
	
	<div style="padding: 6px 0 6px 10px;color: red;">
		<h:outputText value="#{planBean.plan.name}" />
	</div>
	
	<div style="padding: 5px;">
		<div style="float: left;">
			<input id="saveBtn" type="button" class="button" value="保存" disabled="true" onclick="save();" />
			<input type="button" class="button" value="新增任务" onclick="tableMain.table('add');" />
			<input type="button" class="button" value="新增节点" onclick="tableMain.table('add', true);" />
			<input type="button" class="button" value="升级" onclick="tableMain.table('levelUp');" />
			<input type="button" class="button" value="降级" onclick="tableMain.table('levelDown');" />
		</div>
		
		<div style="float: left;padding-left: 18px;padding-bottom: 3px;margin-top: -3px;;">
			<input id="colorBtn" type="text" value="颜色" style="width: 80px;color: #999;" />
		</div>
	</div>
	
	<table id="taskTab" class="list-table" cellpadding="0" cellspacing="0" style="border-top: 1px solid #999;clear: both;">
		<tr>
			<th width="50px" editType="none">序号</th>
			<th width="50px" editType="none">指示灯</th>
			<th width="400px" editType="text">名称</th>
			<th width="120px" editType="select">优先级</th>
			<th width="100px" editType="date">起始日期</th>
			<th width="100px" editType="date">结束日期</th>
			<th width="100px" editType="none">完成率</th>
		</tr>
		
		<a4j:repeat value="#{taskBean.tasks}" var="item" rowKeyVar="row">
		<tr id="<h:outputText value="#{item.id}" />" level="<h:outputText value="#{item.level}" />" 
			isNode="<h:outputText value="#{item.isNode}" />"
			color="<h:outputText value="#{item.color}" />"
			style="background-color: <h:outputText value="#{item.color}" />;">
			
			<td class="sn"><h:outputText value="#{row + 1}" /></td>
			
			<h:panelGroup rendered="#{item.light == 0}">
				<td></td>
			</h:panelGroup>
			<h:panelGroup rendered="#{item.light == 1}">
				<td class="center"><img src="<c:url value="/images/light_green.png" />" /></td>
			</h:panelGroup>
			<h:panelGroup rendered="#{item.light == 2}">
				<td class="center"><img src="<c:url value="/images/light_yellow.png" />" /></td>
			</h:panelGroup>
			<h:panelGroup rendered="#{item.light == 3}">
				<td class="center"><img src="<c:url value="/images/light_red.png" />" /></td>
			</h:panelGroup>
			<h:panelGroup rendered="#{item.light == 4}">
				<td class="center"><img src="<c:url value="/images/light_gray.png" />" /></td>
			</h:panelGroup>
			
			<h:panelGroup rendered="#{item.isNode == 'Y'}">
				<td><div style="font-weight: bold;padding-left: <h:outputText value="#{item.pad}" />px;"><h:outputText value="#{item.name}" /></div></td>
			</h:panelGroup>
			
			<h:panelGroup rendered="#{item.isNode == 'N'}">
				<td><div style="padding-left: <h:outputText value="#{item.pad}" />px;"><h:outputText value="#{item.name}" /></div></td>
			</h:panelGroup>
			
			<td>
				<div val="<h:outputText value="#{item.priority}" />">
					<h:outputText value="#{item.priority}" converter="GoalPrioConverter" />
				</div>
			</td>
			
			<td><div><h:outputText value="#{item.startDate}" converter="DateConverter" /></div></td>
			<td><div><h:outputText value="#{item.endDate}" converter="DateConverter" /></div></td>
			<td><div><h:outputText value="#{item.rate}" />%</div></td>
		</tr>
		</a4j:repeat>
	</table>
	
	<div id="menu"></div>
	
	<div id="priority" class="select-div hide" style="margin-left: -4px;">
		<ul class="menu-ul">
			<li>请选择</li>
			<li val="1">高</li>
			<li val="2">中</li>
			<li val="3">低</li>
		</ul>
	</div>
	
	<h:form id="operate">
		<input type="hidden" id="_info" name="taskInfo" />
		<input type="hidden" name="planId" value="${param.planId}" />
		
		<a4j:commandLink id="save" action="#{taskBean.save}" data="#{taskBean.taskId}" oncomplete="taskAction();" />
	</h:form>
	
	<script>
		var tableMain;
		initTab();
		
		/* task actions */
		var TASK_NONE = 0;
		var TASK_EDIT = 1;
		var TASK_DEL = 2;
		var action = TASK_NONE;
		
		$("#colorBtn").colorpicker().on("change.color", function(event, color) {
			tableMain.table("color", color);
		});
		
		var $menu = $("#menu").omMenu({ 
			contextMenu: true,
			
			dataSource: [{id: "1", label: "发布", action: "publish", seperator: true}, 
			             {id: "2", label: "编辑", action: "edit"}, 
			             {id: "3", label: "删除", action: "del", seperator: true},
			             {id: "4", label: "暂停", action: "pause"},
			             {id: "5", label: "终止", action: "stop"},
			             {id: "6", label: "重新开启", action: "open"}],
			             
			onSelect: function(item, e) {
				var $tr = tableMain.table("getRow"), id = $tr.attr("id");
				$("#saveBtn").attr("disabled", false);
				
				if(item.action == "del") {
					if(!id) { $tr.remove(); } else { $tr.attr("isDel", "Y").hide(); }
				}
				
				if(item.action == "edit") {
					action = TASK_EDIT;  save();
				}
				
				if(item.action == "publish") {
					$tr.attr({"isEdit": "Y", "status": 2});  save();
				}
			}
		});
		
		function initTab() {
			tableMain = $("#taskTab").table({ 
				menu: "menu", 
				nameCol: 2, 
				isEdit: true,
				isDateEdit: true,
				select: "priority", 
				
				controlMenu: function(event, $tr) {
					var menuSize = 6;
					for(var i = 1; i <= menuSize; i++) {
						if($("#" + i).hasClass("om-state-disabled"))  $menu.omMenu("enableItem", i); 
					}
					
					if($tr.attr("isNode") == "Y") {
						$menu.omMenu("disableItem", "2");
					}
				}
			});			
		}
		
		function taskAction(id) {
			if(action == TASK_NONE)  return;
			
			var $tr = tableMain.table("getRow");
			if($tr.attr("id"))  id = $tr.attr("id");
			
			if(!id)  return;
			
			if(action == TASK_EDIT) {
				buildPage("taskFrm", basePath + "pages/goal/taskEdit.jsf?taskId=" + id, true);
			}
			
			action = TASK_NONE;
		}
		
		function save() {
			if($("#saveBtn").attr("disabled")) { taskAction();  return; }
				
			var datas = [], data;
			
			$("#taskTab tr[isDel='Y']").each(function() {
				data = {i: $(this).attr("id")};
				datas.push(data);
			});
			
			$("#taskTab tr[isEdit='Y']").each(function() {
				var $tr = $(this), $pTr = $tr.prev(), $tds = $("td[isEdit='Y']", $tr), data = {};
				
				data.i = $tr.attr("id") ? $tr.attr("id") : "";
				data.nd = $tr.attr("isNode") == "Y" ? "Y" : "N";
				data.ri = $tr[0].rowIndex;
				
				if($tr.attr("parentId"))  data.pi = $tr.attr("parentId");
				if($tr.attr("color"))  data.c = $tr.attr("color");
				if($tr.attr("status"))  data.st = $tr.attr("status");
				if($pTr.attr("id"))  data.snid = $pTr.attr("id");
				
				// if item need id to return
				if(action != TASK_NONE) {
					data.ni = "Y";					
				}
				
				$tds.each(function() {
					var $t = $(this), index = $t[0].cellIndex, $d = $("div", $t), v = $d.text().trim();
					
					if(index == 2) {
						data.n = v;
					}
					else if(index == 3) {
						data.p = $d.attr("val") ? $d.attr("val") : ""
					}
					else if(index == 4) {
						data.s = v;
					}
					else if(index == 5) {
						data.e = v;
					}
				});
				
				datas.push(data);
			});
			
			$("#saveBtn").attr("disabled", true);
			$("#_info").val(JSON.stringify(datas));
			$("#operate\\:save").click();
		}
		
		function refresh(info) {
			if(!info)  return;
			var $tr = $("#" + info.i);
			
			$("td:eq(2) div", $tr).text(info.n);
			$("td:eq(3) div", $tr).attr("val", info.p).text(info.pn);
			$("td:eq(4) div", $tr).text(info.s);
			$("td:eq(5) div", $tr).text(info.e);
		}
	</script>
</body>
</html>
</f:view>