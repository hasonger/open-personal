<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />" type="text/javascript"></script>
	
	<style>
		table tr {height: 40px;}
	</style>
</head>

<body>
	<div style="padding-top: 15%;padding-left: 40%">
		<h:form id="operate">
			<table style="border: 1px solid #999;padding: 30px;border-radius: 15px;">
				<tr>
					<td>用户名：</td>
					<td><input name="username" type="text" class="edit-require" /><span class="require">*</span></td>
				</tr>
				<tr>
					<td>密码：</td>
					<td><input name="password" type="password" class="edit-require" /><span class="require">*</span></td>
				</tr>
				<tr>
					<td>确认密码：</td>
					<td><input type="password" class="edit-require" /><span class="require">*</span></td>
				</tr>
				<tr>
					<td>邮件：</td>
					<td><input name="email" type="password" class="edit-require" /><span class="require">*</span></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top: 20px;text-align: center;">
						<button onclick="register();" class="button" style="margin-right: 5px;">注册</button>
						<a href="../index.jsf" style="font-size: 12px;">返回登录</a>
					</td>
				</tr>
			</table>
		
			<h:commandLink id="register" action="#{userBean.register}" />
		</h:form>
	</div>
	
	<script>
		function register() {
			$("#operate\\:register").click();
		}
	</script>
</body>
</html>
</f:view>