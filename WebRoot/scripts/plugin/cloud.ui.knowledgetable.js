(function($, undefined) {
	
$.widget("ui.table", $.ui.mouse, {
	
	options:{
		menu: null
	},
	
	_create: function() {
		var $s = this, con = $s.element;
		
		// remove left border in first column
		$s._initFirstTdBorder();
		
		// init edit input
		$s._initEditInput();
		
		// init click tr event and click td event
		$s._initClickEvent();
		
		// init context menu
		$s._initContextMenu();
		
		// init drag dotted select area
		$s.line_area = $("<div style='display: none;position: absolute;border: 1px dotted #999;background: transparent;'></div>");
		con.append($s.line_area);
		
		$(document.body).bind("click", function() { $s._blurInput(true); });
		
		$s._mouseInit();
	},
	
	getRow: function() {
		return $("tr.tr-selected", this.element);
	},
	
	addKnowledge: function(isNode) {
		var $s = this, tab = $s.element;
		
		var html = "<tr isEdit='Y' " + (isNode ? "isNode='Y'" : "") + ">";
		html += "<td style='border-left: 0;' class='sn'>" + $("tr", tab).size() + "</td>";
		html += "<td><div" + (isNode ? " style='font-weight: bold;'" : "") + "></div></td>";
		html += "</tr>";
		
		var $tr = $(html).bind("click", function(e) {
			$("tr", tab).removeClass("tr-selected");
			$(this).addClass("tr-selected");
			e.stopPropagation();
		});
		
		$("td:gt(0)", $tr).bind("click", function(e) {
			$s._initTdClick($(this));
		});
		
		tab.append($tr.click());
	},
	
	levelUp: function() {
		var $s = this, tab = $s.element, $trs = $s.getRow();
		if($trs.size() == 0)  return;
		
		var $tr = $trs.eq(0), $pTrs = $tr.prevAll("tr[isNode='Y']"), $pTr;
		$pTrs.each(function() {
			if(parseInt($(this).attr("level")) < parseInt($tr.attr("level")))  $pTr = $(this);
		});
		if(!$pTr || $pTr.size() == 0)  return;
		
		var l = parseInt($tr.attr("level")), pl = parseInt($pTr.attr("level"));
		if(l <= pl)  return;
		
		$trs.each(function() {
			// reset level
			var level = parseInt($(this).attr("level")) - 1;
			$(this).attr("level", level);
			
			// reset padding
			var $d = $("td:eq(1) div", this);
			$d.css("padding-left", (level * 18 + 5) + "px");
			
			// set parent node
			$(this).attr("isEdit", "Y").attr("parentId", level == 0 ? "" : $pTr.attr("id"));
		});
		
		if(!$("#saveBtn").attr("disabled") || $("#saveBtn").attr("disabled") == "disabled")  $("#saveBtn").attr("disabled", false);
	},
	
	levelDown: function() {
		var $s = this, tab = $s.element, $trs = $s.getRow();
		if($trs.size() == 0)  return;
		
		var $tr = $trs.eq(0), $pTr = $tr.prevAll("tr[isNode='Y']").eq(0);
		if($pTr.size() == 0)  return;
		
		var l = parseInt($tr.attr("level")), pl = parseInt($pTr.attr("level"));
		if(l > pl)  return;
		
		$trs.each(function() {
			// reset level
			var level = parseInt($(this).attr("level")) + 1;
			$(this).attr("level", level);
			
			// reset padding
			var $d = $("td:eq(1) div", this);
			$d.css("padding-left", (level * 18 + 5) + "px");
			
			// set parent node
			$(this).attr("isEdit", "Y").attr("parentId", $pTr.attr("id"));
		});
		
		if(!$("#saveBtn").attr("disabled") || $("#saveBtn").attr("disabled") == "disabled")  $("#saveBtn").attr("disabled", false);
	},
	
	color: function(color) {
		var $s = this, tab = $s.element, $trs = $s.getRow();
		if($trs.size() == 0)  return;
		
		$("tr", tab).removeClass("tr-selected");
		$trs.attr("isEdit", "Y").css("background-color", color).attr("color", color);
		
		if(!$("#saveBtn").attr("disabled") || $("#saveBtn").attr("disabled") == "disabled")  $("#saveBtn").attr("disabled", false);
	},
	
	_initEditInput: function() {
		var $s = this, tab = $s.element;
		
		// init text input
		$s.text_input = $("<input type='text' class='hide' style='height: 29px;margin-left: -5px;' />");
		tab.append($s.text_input);
		
		$s.text_input.bind("click", function(e) { e.stopPropagation(); });
	},
	
	_initTdClick: function($td) {
		var $s = this, tab = $s.element;
		
		$s._blurInput(false);
		
		var $d = $("div", $td).hide(), index = $td[0].cellIndex, $input;
		
		$td.append($s.text_input.width($td.width() + 6).val($d.text()).show());
		$s.text_input.click().focus();
	},
	
	_blurInput: function(isBody) {
		var $s = this, tab = $s.element, $input = $s.text_input, $d;
		
		if($input && $input.is(":visible"))  $d = $input.prev();
		if(!$input || $input.size() == 0 || !$d || $d.size() == 0)  return;
		
		// get origin display text and input value
		$input.hide();
		var origin = $d.text();
		var value = $input.val();
		
		// init edit value to display when input blur
		$d.text(value).show();
		
		if(isBody)  tab.append($input);
		
		// enable save button and set edit status to td
		if(!$("#saveBtn").attr("disabled") || $("#saveBtn").attr("disabled") == "disabled")  $("#saveBtn").attr("disabled", false);
		
		if(origin.trim() != value.trim()) {
			$d.parent().attr("isEdit", "Y");
			$d.closest("tr").attr("isEdit", "Y");
		}
	},
	
	_initFirstTdBorder: function() {
		var $s = this, tab = $s.element;
		
		$("tr:eq(0) th:eq(0)", tab).css("border-left", "0");
		
		$("tr:gt(0)", tab).each(function() {
			$("td:eq(0)", $(this)).css("border-left", "0");
		});
	},
	
	_initClickEvent: function() {
		var $s = this, tab = $s.element;
		
		$("tr:gt(0)", tab).click(function(e) {
			$("tr", tab).removeClass("tr-selected");
			$(this).addClass("tr-selected");
			e.stopPropagation();
		});
		
		$("tr:gt(0)", tab).each(function() {
			var $tds = $(this).children().filter("td:gt(0)");
			
			$tds.each(function() {
				$(this).bind("click", function() { $s._initTdClick($(this)); });
			});
		});
	},
	
	_initContextMenu: function() {
		var $s = this, tab = $s.element;
		
		if(!$s.options.menu)  return;
		
		$("tr:gt(0)", tab).bind("contextmenu", function(e) {
			$s._blurInput(true);
			
			// show menu
			$("#" + $s.options.menu).omMenu("show", e);
			
			// select row
			$("tr", tab).removeClass("tr-selected");
			$(this).addClass("tr-selected");
		});
	},
	
	_mouseCapture: function(event) {
		
		return true;
	},
	
	_mouseStart: function(event) {
		var $s = this, tab = $s.element;
		
		$("tr", tab).removeClass("tr-selected");
		
		$s._blurInput(true);
		$s.startPos = {x: event.clientX, y: event.clientY};
		
		$s.line_area.css({"left": event.clientX + "px", "top": event.clientY + "px"}).show();
	},
	
	_mouseDrag: function(event) {
		var $s = this, tab = $s.element, sp = $s.startPos, ep = {x: event.clientX, y: event.clientY};
		$s.endPos = ep;
		
		var x = sp.x < ep.x ? sp.x : ep.x, y = sp.y < ep.y ? sp.y : ep.y;
		var w = Math.abs(sp.x - ep.x), h = Math.abs(sp.y - ep.y);
		
		$s.line_area.css({"left": x + "px", "top": y + "px"}).width(w).height(h);
	},
	
	_mouseStop: function(evnet) {
		var $s = this, tab = $s.element;
		
		$("tr", tab).removeClass("tr-selected");
		var sy = $s.startPos.y < $s.endPos.y ? $s.startPos.y : $s.endPos.y;
		var ey = $s.startPos.y < $s.endPos.y ? $s.endPos.y : $s.startPos.y;
		
		
		$("tr:gt(0)", tab).each(function() {
			var t1 = $(this).offset().top, t2 = t1 + $(this).height();
			
			if((sy < t1 && t1 < ey) || (sy < t2 && t2 < ey) || (sy > t1 && ey < t2)) {
				$(this).addClass("tr-selected");
			}
		});
		
		$s.line_area.hide();
	}
});	
	
})(jQuery);